const express = require('express');
const path = require('path');
const dotenv = require('dotenv');
const logger = require('morgan');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const nodemailer = require('nodemailer');

const webpackDevServer = require('./webpack/dev-server');
const routes = require('./routes');

// use dotenv
dotenv.config({ silent: true });

// Express app setup
const app = express();

// view engine
app.set('views', path.join(__dirname, './views'));
app.set('view engine', 'pug');

// include webpack-dev-server for development only
if (process.env.NODE_ENV !== 'production') {
  webpackDevServer(app);
}

// serve static files from 'public'
app.use(express.static(path.join(__dirname, './public')));

// body parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// use routes
app.use('/', routes);

// post form content to backend
app.post('/api/form', (req, res) => {
  nodemailer.createTestAccount((err, account) => {
    const htmlEmail = `
      <h3>Contact Details</h3>
      <ul>
        <li>Name: ${req.body.name}</li>
        <li>Email: ${req.body.email}</li>
      </ul>
      <h3>Message<h3>
      <p>${req.body.message}</p>
    `;

    // create reusable transporter object using the default SMTP transport
    const transporter = nodemailer.createTransport({
      host: 'smtp.ethereal.email',
      port: 587,
      secure: false, // true for 465, false for other ports
      auth: {
        user: 'qvhzbnaxrjm2dxrk@ethereal.email', // generated ethereal user
        pass: 'UNdVMnfxMaMqMQRGxx', // generated ethereal password
      },
    });

    // setup email data with unicode symbols
    const mailOptions = {
      from: req.body.email, // sender address
      to: 'qvhzbnaxrjm2dxrk@ethereal.email', // list of receivers
      replyTo: req.body.email,
      subject: 'New Message', // Subject line
      text: req.body.message, // plain text body
      html: htmlEmail, // html body
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        return console.log(error);
      }
      console.log('Message sent: %s', info.messageId);
      // Preview only available when sending through an Ethereal account
      console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

      // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
      // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
      return res.status;
    });
  });
});

// logger
app.use(logger('combined'));

// cookie parser
app.use(cookieParser());

// error handlers
app.use((err, req, res, next) => {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: app.get('env') === 'development' ? err : {},
  });
  next();
});

// catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// export default app;
module.exports = app;
