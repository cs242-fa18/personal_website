import React from 'react';
import {
  Form, FormGroup, Input, Label, Alert,
} from 'reactstrap';
import { Button, Message } from 'semantic-ui-react';
import axios from 'axios';

import Header from '../components/Header.jsx';
import styles from '../styles/Contact.scss';
import 'bootstrap/dist/css/bootstrap.css';
import Copyright from '../components/Copyright.jsx';

class Contact extends React.Component {
    state = {
      activeItem: 'Contact',
      name: '',
      email: '',
      message: '',
      open: false,
    }

    handleChangeInput = (e) => {
      this.setState({ [e.target.name]: e.target.value });
    }

    handleDismissAlert = () => this.setState({ open: false })

    async handleSendEmail(e) {
      this.setState({ open: true });

      e.preventDefault();

      const { name, email, message } = this.state;
      const _ = await axios.post('/api/form', {
        name,
        email,
        message,
      });
    }

    render() {
      const { activeItem, open } = this.state;

      return (
        <div className="contact">
          <Header id="header" activeItem={activeItem} />
          <div className="form-container">
            <Message
              id="message"
              header="Contact me"
              content="Drop me a quick email to ask me any questions about my projects, experiences, or art works."
            />
            <Form>
              <FormGroup>
                <Label for="name">Name</Label>
                <Input
                  type="text"
                  name="name"
                  placeholder="Dorothy Yu"
                  onChange={this.handleChangeInput.bind(this)}
                />
              </FormGroup>
              <FormGroup>
                <Label for="email">Email</Label>
                <Input
                  type="text"
                  name="email"
                  placeholder="example@domain.com"
                  onChange={this.handleChangeInput.bind(this)}
                />
              </FormGroup>
              <FormGroup>
                <Label for="message">Message</Label>
                <Input
                  type="textarea"
                  name="message"
                  placeholder="Share your thoughts :)"
                  onChange={this.handleChangeInput.bind(this)}
                />
              </FormGroup>
              <Button
                basic
                color="black"
                content="Send"
                onClick={this.handleSendEmail.bind(this)}
              />
            </Form>
            <Alert id="alert" color="success" isOpen={open} toggle={this.handleDismissAlert}>
                Your message has been sent. Thank you!
            </Alert>
          </div>
          <Copyright />
        </div>
      );
    }
}

export default Contact;
