import React from 'react';
import { Button } from 'semantic-ui-react';
import { Modal, ModalBody, ModalFooter } from 'reactstrap';
import Typing from 'react-typing-animation';

import Header from '../components/Header.jsx';
import Copyright from '../components/Copyright.jsx';
import styles from '../styles/About.scss';
import resume from '../../public/assets/files/resume_fa18.pdf';

class About extends React.Component {
    state = {
      activeItem: 'About',
      open: false,
    }

    handleOpenModal = (e) => {
      this.setState({
        open: true,
      });
    }

    handleCloseModal = e => this.setState({ open: false })

    render() {
      const { activeItem, open } = this.state;

      return (
        <div className="about">
          <Header id="header" activeItem={activeItem} />
          <div className="about-content">
            <img alt="" id="about-self" src={require('../../public/assets/images/about_self.jpg')} />
            <div className="intro">
              <p id="title">Jingxuan(Dorothy) Yu - Software Engineer</p>
              <p id="sub">Born on April 30th, 1996, China</p>
              <div id="text">
                <p id="no-margin-bottom">Education</p>
                <p>2018, BS, Computer Science, University of Illinois at Urbana-Champaign, US</p>
                <p id="no-margin-bottom">Experiences</p>
                <p>HUAWEI - Summer Intern: worked as a data analyst for building ML models to experimentally predict program stucking reasons.</p>
                <p id="no-margin-bottom">Projects</p>
                <p id="no-margin">
Fashion Web App - DataDrivenDesign Group: worked as a front-end developer.
                  <a id="github-link" href="https://github.com/likedan/Production-VocabApp">Github</a>
                </p>
                <p>
Chrome Extension - DA Algorithm: worked to develop the similarity function used to compute sentiment score among YouTube comments.
                  <a id="github-link" href="https://github.com/herbherbherb/CS410-Youtube-Review-Miner">Github</a>
                  <a id="github-link" href="https://www.youtube.com/watch?v=yUv-jiRFoPw&feature=youtu.be">Demo</a>
                </p>
                <Button
                  id="button"
                  content="View My Resume for More ..."
                  basic
                  color="black"
                  onClick={this.handleOpenModal.bind(this)}
                />
              </div>
            </div>
          </div>
          <Modal size="lg" isOpen={open} toggle={this.handleCloseModal.bind(this)}>
            <ModalBody>
              <embed src={resume} type="application/pdf" height={700} width="100%" />
            </ModalBody>
            <ModalFooter>
              <Button
                basic
                content="Close"
                color="black"
                onClick={this.handleCloseModal.bind(this)}
              />
            </ModalFooter>
          </Modal>
          <div id="auto-typing">
            <Typing speed={150} loop>
              <span id="auto-font">I didn't put much stuff here.</span>
              <Typing.Reset count={1} delay={500} />
              <span id="auto-font">Because EVERYONE is doing so.</span>
              <Typing.Reset count={1} delay={500} />
              <span id="auto-font">It might be useful, but NOT FUN at all.</span>
              <Typing.Reset count={1} delay={500} />
              <span id="auto-font">This is not 'super' fun though.</span>
              <Typing.Reset count={1} delay={500} />
              <span id="auto-font">But at least it feels like we're talking to each other.</span>
              <Typing.Reset count={1} delay={500} />
              <span id="auto-font">How are you :)</span>
              <Typing.Reset count={1} delay={1000} />
            </Typing>
          </div>
          <Copyright />
        </div>
      );
    }
}

export default About;
