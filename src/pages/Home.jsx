import React from 'react';

import Header from '../components/Header.jsx';
import styles from '../styles/Home.scss';
import Copyright from '../components/Copyright.jsx';

class Home extends React.Component {
    state = {
      activeItem: 'Home',
    }

    render() {
      const { activeItem } = this.state;

      return (
        <div className="home">
          <Header id="header" activeItem={activeItem} />
          <div className="intro-container">
            <div id="intro-title">
              <p>Hi! I&apos;m Dorothy Yu.</p>
            </div>
            <div id="intro-sub">
              <p>A CS student in UIUC.</p>
            </div>
          </div>
          <div id="home-copyright">
            <Copyright />
          </div>
        </div>
      );
    }
}

export default Home;
