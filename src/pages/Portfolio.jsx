import React from 'react';
import { Card, Button, Image } from 'semantic-ui-react';
import {
  Modal, ModalHeader, ModalFooter, ModalBody,
} from 'reactstrap';
import {
  Link, DirectLink, Element, Events, animateScroll as scroll, scrollSpy, scroller,
} from 'react-scroll';

import Header from '../components/Header.jsx';
import CardCarousel from '../components/CardCarousel.jsx';
import styles from '../styles/Portfolio.scss';
import items from '../data/data.json';
import Copyright from '../components/Copyright.jsx';

class Portfolio extends React.Component {
    state = {
      activeItem: 'Portfolio',
      modalName: '',
      modalId: [],
      modalDescription: [],
      open: false,
    }

    handleClickCard = (name, id, des) => this.setState({
      modalName: name, modalId: id, modalDescription: des, open: true,
    })

    handleCloseCard = () => this.setState({ open: false })

    scrollToTop = () => {
      scroll.scrollToTop();
    }

    render() {
      const {
        activeItem, open, modalName, modalId, modalDescription,
      } = this.state;

      return (
        <div className="portfolio">
          <Header id="header" activeItem={activeItem} />
          <div className="gallery">
            <div className="gallery-intro">
              <p id="gallery-title">NOT-A-TECH-PORT PORTFOLIO</p>
              <p id="gallery-sub">In Scientia Veritas, in arte honestas. -- Anonymous</p>
            </div>
            <div className="card-container">
              <Card.Group id="card-group">
                {items.map((item, idx) => (
                  <Card
                    image={require(`../../public/assets/images/${item.id}-1.JPG`)}
                    header={item.name}
                    meta={item.time}
                    onClick={this.handleClickCard.bind(this, item.name, item.id, item.description)}
                    key={item.id}
                  />
                ))}
              </Card.Group>
            </div>
            <Modal isOpen={open} toggle={this.handleCloseCard}>
              <ModalHeader toggle={this.handleCloseCard}>{modalName}</ModalHeader>
              <ModalBody>
                <CardCarousel idx={modalId} />
                <div id="description">
                  {modalDescription.map((line, key) => (
                    <h5 key={key}>{line}</h5>
                  ))}
                </div>
              </ModalBody>
              <ModalFooter>
                <Button basic color="black" onClick={this.handleCloseCard}>Close</Button>
              </ModalFooter>
            </Modal>
          </div>
          <div id="back-to-top">
            <a onClick={this.scrollToTop}>
              <Image height={50} width={50} src={require('../../public/assets/images/up-arrow.jpg')} />
            </a>
          </div>
          <Copyright />
        </div>
      );
    }
}

export default Portfolio;
