import React from 'react';
import { render } from 'react-dom';
import {
  HashRouter as Router, Route, Switch,
} from 'react-router-dom';
import 'semantic-ui-css/semantic.min.css';

import Home from './pages/Home.jsx';
import About from './pages/About.jsx';
import Portfolio from './pages/Portfolio.jsx';
import Contact from './pages/Contact.jsx';

render(
  <Router>
    <Switch>
      <Route exact path="/" component={Home} />
      <Route exact path="/About" component={About} />
      <Route exact path="/Portfolio" component={Portfolio} />
      <Route exact path="/Contact" component={Contact} />
    </Switch>
  </Router>,
  document.getElementById('app'),
);
