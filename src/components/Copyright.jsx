import React from 'react';

import styles from '../styles/Copyright.scss';

class Copyright extends React.Component {
  render() {
    return (
      <div className="copyright">
        <p id="copyright-text">Copyrights © 2018 Jingxuan Yu. All rights reserved.</p>
      </div>
    );
  }
}

export default Copyright;
