import React from 'react';
import { Menu, Image } from 'semantic-ui-react';

import styles from '../styles/Header.scss';

class Header extends React.Component {
    changeHeaderItem = (event, { name }) => {
      if (name !== 'Home') {
        window.location = `#/${name}`;
      } else {
        window.location = '#/';
      }
    }

    render() {
      const { activeItem } = this.props;

      return (
        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
          <Menu tabular id="header-self" style={{ flexGrow: '1' }}>
            <Menu.Item name="Home" active={activeItem === 'Home'} onClick={this.changeHeaderItem} />
            <Menu.Item name="About" active={activeItem === 'About'} onClick={this.changeHeaderItem} />
            <Menu.Item name="Portfolio" active={activeItem === 'Portfolio'} onClick={this.changeHeaderItem} />
            <Menu.Item name="Contact" active={activeItem === 'Contact'} onClick={this.changeHeaderItem} />
          </Menu>
          <Image src={require('../../public/assets/images/signature.png')} height={100} width={200} />
        </div>
      );
    }
}

export default Header;
