# Dorothy's Personal Website
This is the final project for CS242. 

# Run a Specific Package Locally
Use `./node_modules/.bin/package-name --[options]`.

# Read `.editorconfig` File before Continuing Development
`.editorconfig` specifies some general style rules. Use it as a side guide.

# Citations:
1. https://medium.com/@TeeFouad
2. https://dev.to/willamesoares/how-to-build-an-image-carousel-with-react--24na
