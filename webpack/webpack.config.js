const path = require('path');
const webpack = require('webpack');

module.exports = {
  entry: [
    '@babel/polyfill',
    path.join(process.cwd(), 'src/index'),
  ],
  output: {
    filename: 'bundle.js',
    path: path.join(process.cwd(), 'dist'),
    publicPath: '/',
  },
  plugins: [
    new webpack.NoEmitOnErrorsPlugin(),
  ],
  module: {
    rules: [
      {
        test: /\.(js|jsx)?$/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env', '@babel/preset-react'],
            plugins: ['transform-inline-environment-variables'],
          },
        },
        exclude: /node_modules/,
      },
      {
        enforce: 'pre',
        test: /\.js$/,
        use: 'eslint-loader',
      },
      {
        test: /\.(s)?css$/,
        use: ['style-loader', 'css-loader', 'sass-loader'],
      },
      {
        test: /\.svg$/,
        use: 'svg-inline-loader',
      },
      {
        test: /\.(jpe?g|png|gif|ico|ttf|woff|eot|woff2)$/i,
        use: 'url-loader',
      },
      {
        test: /\.pdf$/,
        use: 'file-loader',
      },
    ],
  },
  target: 'web',
  mode: 'development',
};
