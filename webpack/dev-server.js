const webpack = require('webpack');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackConfig = require('./webpack.config');

module.exports = (app) => {
  const webpackCompiler = webpack(webpackConfig);
  // use dev middleware
  app.use(webpackDevMiddleware(webpackCompiler, {
    // defines the level of messages to log
    logLevel: 'debug',
    // public path to bind the middleware to
    publicPath: webpackConfig.output.publicPath,
  }));
};
